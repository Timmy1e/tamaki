FROM node:8.4

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Add app dependency files
COPY package.json /usr/src/app/
COPY package-lock.json /usr/src/app/

# Set Node to production
ENV NODE_ENV=production

# Install dependencies
RUN npm install

# Bundle app source
COPY . /usr/src/app/

EXPOSE 3000
CMD [ "npm", "start" ]