# Tamaki
Tamaki is a relatively lightweight dashboard.
It displays the current time and date,
	upcoming events through Google calendar,
	and news items through a NOS RSS feed.
It's a Docker ready NodeJS web app, using Express and EJS, with a AngularJS frontend.
In this configuration it runs on a RaspberryPi 2B, and is setup for the Dutch language.

## Deployment
Please use the `production` branch.
A README will be available there.
