const path = require('path');
const redis = require('express-redis-cache');
const logger = require('morgan');
const express = require('express');
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const sassMiddleware = require('node-sass-middleware');
const babelMiddleware = require('express-babel').default;
const PROD = process.env.NODE_ENV === 'production';

const app = express();

/// Redis cache
global.cache = redis({
	prefix: 'tamaki',
	host: process.env.x_tamaki_redis_host,
	expire: process.env.x_tamaki_redis_expire ? parseInt(process.env.x_tamaki_redis_expire) : 900
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'images', 'tamaki.png')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(sassMiddleware({
	src: path.join(__dirname, 'public'),
	dest: path.join(__dirname, 'public'),
	indentedSyntax: false,
	outputStyle: 'compressed',
	sourceMap: false
}));
app.use('/scripts', babelMiddleware(path.join(__dirname, 'public', 'scripts'), {
	compact: true,
	minified: true,
	presets: ['env', 'minify']
}));
app.use(express.static(path.join(__dirname, 'public')));

addStaticDep('angular');
addStaticDep('angular-animate');
addStaticDep('bootstrap', path.join('bootstrap', 'dist'));
addStaticDep('jquery', path.join('jquery', 'dist'));
addStaticDep('popper', path.join('popper.js', 'dist', 'umd'));
addStaticDep('moment', path.join('moment', 'min'));


function addStaticDep(depName, depPath = depName) {
	app.use('/dist/' + depName, express.static(path.join(__dirname, 'node_modules', depPath)));
}

app.get('/robots.txt', global.cache.route(), (req, res) => {
	res.type('text/plain');
	res.send(
		'User-agent: *\n' +
		'Disallow: /'
	);
});

app.use('/', require('./routes/index'));
app.use('/google/', require('./routes/google'));

// catch 404 and forward to error handler
app.use((req, res, next) => {
	let err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use((err, req, res, next) => {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = PROD ? {} : err;

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;
