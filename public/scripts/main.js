/*global angular, moment */
angular.module('tamaki', ['ngAnimate']);
angular.module('tamaki').controller('controller', ['$scope', '$http', '$timeout', '$window', ($scope, $http, $timeout, $window) => {
	const LOCAL_STORAGE_KEY = 'xyz.archbox.tamaki.google';
	const CLOCK_TICK_INTERVAL = 1000; // 1 sec
	const TOKEN_MINIMUM_TIME_LEFT = 300; // 5 min
	const WEB_RETRY_INTERVAL = 1000 * 60 * 5; // 5 min
	let calendarItemsLeft = 0;
	let calendarItemsTemp = [];

	function init() {
		$scope.clock = {};
		$scope.newsTime = null;
		$scope.newsItems = [];
		$scope.newsError = null;
		$scope.calendarTime = null;
		$scope.calendarItems = [];
		$scope.calendarError = null;
	}

	init();

	$scope.getLastUpdatedTime = () => {
		return moment().format('HH:mm');
	};

	$scope.getRelativeTime = (dateTime) => {
		return moment(dateTime).locale('nl').fromNow();
	};

	$scope.getCalendarTime = (startDateTimeObj, endDateTimeObj) => {
		let now = moment();
		let startDateTime = getCalendarDateOrDateTimeToMoment(startDateTimeObj);
		let endDateTime = getCalendarDateOrDateTimeToMoment(endDateTimeObj);

		if (now.seconds() <= 30) {
			if (startDateTime.diff(now) > 0) {
				return 'begint ' + startDateTime.fromNow();
			} else {
				return 'eindigt ' + endDateTime.fromNow();
			}
		} else {
			if (endDateTime.diff(startDateTime, 'd') > 0) {
				return startDateTime.format('Y-MM-DD') + ' - ' + endDateTime.format('Y-MM-DD');
			} else {
				return startDateTime.format('LT') + ' - ' + endDateTime.format('LT');
			}
		}
	};

	$scope.calendarLocationStyle = (calendarItemId) => {
		let dateElement = document.getElementById('calendar_' + calendarItemId + '_date');
		let dateWidth = parseFloat(window.getComputedStyle(dateElement).width);
		let maxWidth = dateWidth + 10;

		return {'max-width': 'calc(100% - ' + maxWidth + 'px)'};
	};

	$scope.initGoogleCalendar = (googleCalendarTokenStr) => {
		if (googleCalendarTokenStr) {
			let googleCalendarTokenObj = JSON.parse(atob(googleCalendarTokenStr));
			googleCalendarTokenObj.expiryDate = moment().add(googleCalendarTokenObj.expires_in, 'ms').toISOString();
			setGoogleCalendarTokenObj(googleCalendarTokenObj);
			window.history.pushState('object or string', 'Title', '/' + window.location.href.substring(window.location.href.lastIndexOf('/') + 1).split('?')[0]);
		}
		getCalendar();
	};

	function tickClock() {
		let now = moment().locale('nl');
		$scope.clock.week = now.format('wo dddd');
		$scope.clock.date = now.format('LL');
		$scope.clock.time = now.format('LTS');
		$timeout(tickClock, CLOCK_TICK_INTERVAL);
	}

	function getGoogleCalendarTokenObj() {
		let value = $window.localStorage.getItem(LOCAL_STORAGE_KEY);
		return value ? JSON.parse(atob(value)) : {};
	}

	function setGoogleCalendarTokenObj(googleCalendarTokenObj) {
		return $window.localStorage.setItem(LOCAL_STORAGE_KEY, btoa(JSON.stringify(googleCalendarTokenObj)));
	}

	function getToken(callback) {
		if (!navigator.onLine) {
			$scope.calendarError = 'No internet connection, retying in ' + moment.duration(WEB_RETRY_INTERVAL, 'ms').humanize() + '.';
			console.debug('getToken: No internet');
			$timeout(() => getToken(callback), WEB_RETRY_INTERVAL);
			return;
		}

		if (typeof(callback) !== 'function') {
			return console.error('Callback wasn\'t a function', callback);
		}

		let googleCalendarTokenObj = getGoogleCalendarTokenObj();

		console.debug('Getting token', callback);

		$http({
			url: '/google/token',
			method: 'POST',
			dataType: 'json',
			data: {
				'refresh_token': googleCalendarTokenObj.refresh_token
			}
		}).then(
			(response) => { // Success
				googleCalendarTokenObj.access_token = saveGet([response.data.access_token, googleCalendarTokenObj.access_token]);
				googleCalendarTokenObj.token_type = saveGet([response.data.token_type, googleCalendarTokenObj.token_type]);
				googleCalendarTokenObj.expires_in = saveGet([response.data.expires_in, googleCalendarTokenObj.expires_in]);
				googleCalendarTokenObj.refresh_token = saveGet([response.data.refresh_token, googleCalendarTokenObj.refresh_token]);
				googleCalendarTokenObj.expiryDate = moment().add(googleCalendarTokenObj.expires_in, 's').toISOString();
				console.debug('Success getToken', googleCalendarTokenObj);
				setGoogleCalendarTokenObj(googleCalendarTokenObj);
				callback();
			},
			(response) => { // Error
				console.error('Fail getToken', response);
				$scope.calendarError = 'An error occurred while trying to authenticate against Google, will retry in ' + moment.duration(WEB_RETRY_INTERVAL, 'ms').humanize() + '.';
				$timeout(() => getToken(callback), WEB_RETRY_INTERVAL);
			}
		);
	}

	function getCalendar() {
		if (!navigator.onLine) {
			$scope.calendarError = 'No internet connection, retying in ' + moment.duration(WEB_RETRY_INTERVAL, 'ms').humanize() + '.';
			console.debug('getCalendar: No internet');
			$timeout(getCalendar, WEB_RETRY_INTERVAL);
			return;
		}

		let googleCalendarTokenObj = getGoogleCalendarTokenObj();

		if (!googleCalendarTokenObj || moment(googleCalendarTokenObj.expiryDate).diff(moment()) < TOKEN_MINIMUM_TIME_LEFT) {
			console.debug('getCalendar: Token is expired', moment(googleCalendarTokenObj.expiryDate).diff(moment()));
			return getToken(getCalendar);
		}

		$http({
			url: 'https://www.googleapis.com/calendar/v3/users/me/calendarList',
			method: 'GET',
			dataType: 'json',
			crossDomain: true,
			headers: {
				'Authorization': 'Bearer ' + googleCalendarTokenObj.access_token
			}
		}).then(
			(response) => { // Success
				// console.log('Success getCalendar', response.data);
				$scope.calendarError = null;
				calendarItemsTemp = [];
				calendarItemsLeft = response.data.items.length;
				response.data.items.forEach((item) => {
					getEvents(item.id, item.foregroundColor, item.backgroundColor);
				});
				$timeout(getCalendar, WEB_RETRY_INTERVAL);
			},
			(response) => { // Error
				console.error('Fail getCalendar', response);
				$scope.calendarError = 'An error occurred while trying to get calendars, will retry in ' + moment.duration(WEB_RETRY_INTERVAL, 'ms').humanize() + '.';
				if (response.status === 401) {
					getToken(getCalendar);
				} else {
					$timeout(getCalendar, WEB_RETRY_INTERVAL);
				}
			}
		);
	}

	function getEvents(calendarId, foregroundColor, backgroundColor) {
		if (!navigator.onLine) {
			$scope.calendarError = 'No internet connection, retying in ' + moment.duration(WEB_RETRY_INTERVAL, 'ms').humanize() + '.';
			console.debug('getEvents: No internet');
			$timeout(() => getEvents(calendarId, foregroundColor, backgroundColor), WEB_RETRY_INTERVAL);
			return;
		}

		let googleCalendarTokenObj = getGoogleCalendarTokenObj();

		if (!googleCalendarTokenObj || moment(googleCalendarTokenObj.expiryDate).diff(moment()) < TOKEN_MINIMUM_TIME_LEFT) {
			console.debug('getEvents: Token is expired', moment(googleCalendarTokenObj.expiryDate).diff(moment()));
			return getToken(() => getEvents(calendarId, foregroundColor, backgroundColor));
		}

		$http({
			url: 'https://www.googleapis.com/calendar/v3/calendars/' + encodeURIComponent(calendarId) + '/events' +
			'?timeMin=' + encodeURIComponent(moment().utc().format()) +
			'&timeMax=' + encodeURIComponent(moment().add(1, 'M').utc().format()) +
			'&showDeleted=false' +
			'&singleEvents=true',
			method: 'GET',
			dataType: 'json',
			crossDomain: true,
			headers: {
				'Authorization': 'Bearer ' + googleCalendarTokenObj.access_token
			}
		}).then(
			(response) => { // Success
				// console.log('Success getEvents', calendarItemsLeft, response.data);
				$scope.calendarTime = moment().format('HH:mm');

				response.data.items.forEach((item) => {
					item.foregroundColor = foregroundColor;
					item.backgroundColor = backgroundColor;
				});

				calendarItemsTemp = calendarItemsTemp.concat(response.data.items);

				calendarItemsLeft--;
				if (calendarItemsLeft === 0) {
					sortCalendarItems();
				}
			},
			(response) => { // Error
				console.error('Fail getEvents', calendarItemsLeft, response);
				$scope.calendarError = 'An error occurred while trying to get calendar events, will retry in ' + moment.duration(WEB_RETRY_INTERVAL, 'ms').humanize() + '.';
				$timeout(() => getEvents(calendarId, foregroundColor, backgroundColor), WEB_RETRY_INTERVAL);
			}
		);
	}

	function getNews() {
		if (!navigator.onLine) {
			$scope.newsError = 'No internet connection, retying in ' + moment.duration(WEB_RETRY_INTERVAL, 'ms').humanize() + '.';
			console.debug('getNews: No internet');
			$timeout(getNews, WEB_RETRY_INTERVAL);
			return;
		}

		$http({
			url: 'https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Ffeeds.nos.nl%2Fnosnieuwsalgemeen',
			type: 'GET',
			dataType: 'json',
			crossDomain: true
		}).then(
			(response) => { // Success
				if (response.data.status === 'ok') {
					$scope.newsTime = moment().format('HH:mm');
					$scope.newsError = null;

					response.data.items.sort((a, b) => sortByDate(a.pubDate, b.pubDate));

					$scope.newsItems = response.data.items;
				} else {
					console.warn('Not OK', response.data);
					$scope.newsError = 'An error occurred while trying to parse news, retying in ' + moment.duration(WEB_RETRY_INTERVAL, 'ms').humanize() + '.';
				}
				$timeout(getNews, WEB_RETRY_INTERVAL);
			},
			(response) => { // Error
				$scope.newsError = 'An error occurred while trying to get news, retying in ' + moment.duration(WEB_RETRY_INTERVAL, 'ms').humanize() + '.';
				console.error('Fail getNews', response);
				$timeout(getNews, WEB_RETRY_INTERVAL);
			}
		);
	}

	function saveGet(options) {
		// Declaration needs to be split for minimisation to work.
		let result;
		result = null;
		for (let index = 0; index < options.length; index++) {
			let value = options[index];

			if (typeof(value) === 'function') {
				value = value();
			}

			if (value) {
				result = value;
				break;
			}
		}
		return result;
	}

	function sortByDate(a, b) {
		return moment(b).diff(moment(a));
	}

	function sortCalendarItems() {
		calendarItemsTemp.sort((a, b) => {
			if (!a || !a.start) {
				return 1;
			} else if (!b || !b.start) {
				return -1;
			}

			return sortByDate(b.start.dateTime ? b.start.dateTime : b.start.date, a.start.dateTime ? a.start.dateTime : a.start.date);
		});

		$scope.calendarItems = calendarItemsTemp.slice(0);
		calendarItemsTemp = [];
	}

	function getCalendarDateOrDateTime(dateTimeObj) {
		return dateTimeObj.dateTime ? dateTimeObj.dateTime : dateTimeObj.date;
	}

	function getCalendarDateOrDateTimeToMoment(calendarDateTimeOrDate) {
		return moment(getCalendarDateOrDateTime(calendarDateTimeOrDate)).locale('nl');
	}

	tickClock();
	getNews();

}]);
