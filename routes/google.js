const express = require('express');
const request = require('request');
const router = express.Router();


const GOOGLE = {
	CLIENT: {
		ID: process.env.x_tamaki_google_clientid,
		SECRET: process.env.x_tamaki_google_clientsecret,
	},
	URI: {
		REDIRECT: process.env.x_tamaki_google_baseuri + '/google/callback/',
		TOKEN: 'https://www.googleapis.com/oauth2/v4/token'
	},
	SCOPE: 'https://www.googleapis.com/auth/calendar.readonly',
	GRANT_TYPE: {
		AUTHORIZATION: 'authorization_code',
		REFRESH: 'refresh_token'
	},
	ACCESS_TYPE: 'offline',
	HEADERS: {
		'content-type': 'application/x-www-form-urlencoded',
		'accept': 'application/json',
		'cache-control': 'no-cache'
	}
};

/**
 * GET
 * /google/callback
 */
router.get('/callback', (req, res, next) => {
	let googleAuthorizationCode = req.query.code;
	if (!googleAuthorizationCode) {
		res.status(400).json({ 'error': '`code` missing form url query' }).end();
	}

	request.post({
		url: GOOGLE.URI.TOKEN,
		headers: GOOGLE.HEADERS,
		form: {
			'code': googleAuthorizationCode,
			'redirect_uri': GOOGLE.URI.REDIRECT,
			'client_id': GOOGLE.CLIENT.ID,
			'client_secret': GOOGLE.CLIENT.SECRET,
			'scope': GOOGLE.URI.SCOPE,
			'grant_type': GOOGLE.GRANT_TYPE.AUTHORIZATION,
			'access_type': GOOGLE.AUTHORIZATION
		}
	}, (error, response, body) => {
		let tokenObj = JSON.parse(body);
		let tokenStr = JSON.stringify(tokenObj);

		if (error) {
			console.log(error, response, body);
		}

		res.redirect('/?google_token=' + new Buffer(tokenStr).toString('base64'));
	});
});

/**
 * POST
 * /google/token
 */
router.post('/token', (req, res, next) => {
	let googleRefreshToken = req.body.refresh_token;
	if (!googleRefreshToken) {
		return res.status(400).json({ 'error': '`refresh_token` missing from request body' }).end();
	}

	request.post({
		url: GOOGLE.URI.TOKEN,
		headers: GOOGLE.HEADERS,
		form: {
			'refresh_token': googleRefreshToken,
			'client_id': GOOGLE.CLIENT.ID,
			'client_secret': GOOGLE.CLIENT.SECRET,
			'grant_type': GOOGLE.GRANT_TYPE.REFRESH,
			'access_type': GOOGLE.AUTHORIZATION
		}
	}, (error, response, body) => {
		let tokenObj = JSON.parse(body);
		let tokenStr = JSON.stringify(tokenObj);

		if (error) {
			console.log(error, response, body);
		}

		res.status(response.statusCode).send(tokenStr);
	});
});

/**
 * GET
 * /google/redirect
 */
router.get('/redirect', global.cache.route(), (req, res, next) => {
	let redirectUri = 'https://accounts.google.com/o/oauth2/v2/auth' +
		'?scope=' + encodeURIComponent(GOOGLE.SCOPE) +
		'&access_type=offline' +
		'&prompt=consent' +
		'&include_granted_scopes=true' +
		'&state=state_parameter_passthrough_value' +
		'&redirect_uri=' + encodeURIComponent(GOOGLE.URI.REDIRECT) +
		'&response_type=code' +
		'&client_id=' + encodeURIComponent(GOOGLE.CLIENT.ID);

	res.redirect(redirectUri);
});

module.exports = router;
