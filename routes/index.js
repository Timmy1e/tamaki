const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', global.cache.route(), (req, res, next) => {
	let googleToken = req.query.google_token;
	googleToken = googleToken ? googleToken : null;

	res.render('index', { googleToken: googleToken });
});

module.exports = router;
